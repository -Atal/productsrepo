import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { NgxsLoadingPluginModule } from 'ngxs-loading-plugin';
import { NgxsModule } from '@ngxs/store';
import { ProductState } from './store/product.state';

import { ProductsRoutingModule } from './products-routing.module';
import { SharedModule } from '../shared/shared.module';

import { ProductsComponent } from './products-main/products.component';
import { ProductsTemplateComponent } from './products-template/products-template.component';
import { FilterFormComponent } from './filter-form/filter-form.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductFromComponent } from './product-from/product-from.component';


@NgModule({
    declarations:[
        ProductsComponent,
        ProductsTemplateComponent,
        FilterFormComponent,
        ProductDetailsComponent,
        ProductFromComponent,
    ],
    
    imports:[
        SharedModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        ProductsRoutingModule,
        NgxsLoadingPluginModule,
        NgxsModule.forFeature([ProductState])
    ],

    exports:[
        ProductsComponent,
        ProductsTemplateComponent,
        FilterFormComponent,
        ProductDetailsComponent,
        ProductFromComponent,
    ],
})

export class ProductsModule{}