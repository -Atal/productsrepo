import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { Location } from '@angular/common';
import { of, Subject } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';

import { ProductsTemplateComponent } from './products-template.component';
import { ProductService } from '../services/product.service';
import { FilterFormComponent } from '../filter-form/filter-form.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { IProducts } from 'src/app/shared/interfaces/productInterface';

describe('ProductsTemplateComponent', () => {
  let component: ProductsTemplateComponent;
  let fixture: ComponentFixture<ProductsTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsTemplateComponent, FilterFormComponent ],
      imports: [ MaterialModule, FormsModule, BrowserAnimationsModule,
        RouterTestingModule.withRoutes([
          {path: 'product/new', component: DummyComponent},
          {path: 'product/update', component: DummyComponent},
          {path: 'product/:id/details', component: DummyComponent}
        ])
      ],
      providers: [ 
        { provide: ProductService, useClass: ProductServiceStub},
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an array that contains the following headers', () => {
    expect(component.columnsToDisplay).toContain('id')
    expect(component.columnsToDisplay).toContain('name')
    expect(component.columnsToDisplay).toContain('details')
    expect(component.columnsToDisplay).toContain('update')
    expect(component.columnsToDisplay).toContain('delete')
  })

  it('should show productTemplateComponent(this one) if root or / in navigation', () => {
    const location = TestBed.get(Location)
    expect(location.path()).toBe('')
  })

  it('should navigate to product/details when details link is clicked', fakeAsync (()=> {
    const location = TestBed.get(Location)
    const link = fixture.debugElement.query(By.css('a'))
    const linkEle: HTMLAnchorElement = link.nativeElement
    linkEle.click()
    tick()
    fixture.detectChanges()
    // expect(location.path()).toBe('/product/1/details')
    expect(location.path()).toContain('/product','/details')
  }))

  it('should navigate to product/new when Add New Product button is clicked', fakeAsync (() => {
    const location = TestBed.get(Location)
    const button = fixture.debugElement.query(By.css('#addProduct'))
    const buttonEle: HTMLButtonElement = button.nativeElement
    buttonEle.click()
    tick()
    fixture.detectChanges()
    expect(location.path()).toBe('/product/new')
  }))

  it('should navigate to product/update when Update button is clicked', fakeAsync (() => {
    const location = TestBed.get(Location)
    const buttons = fixture.debugElement.queryAll(By.css('button.update'))
    let updateButton: HTMLButtonElement
    if(buttons.length){
      updateButton = buttons[0].nativeElement
    }
    updateButton.click()
    tick()
    fixture.detectChanges()
    expect(location.path()).toBe('/product/update')
  }))

})

export class ProductServiceStub{
  productsArray: IProducts [] = []
  subject = new Subject<IProducts[]>()

  constructor(){
    this.getallProducts().subscribe((data => {
      this.productsArray = data
    }))
    this.subject.next(this.productsArray)
  }
  getallProducts(){
    return of([
      {
        id: 1,
        name: 'arsenal',
        price: 2000,
        description: 'A football club',
        key: '21312312random'
      },
      {
        id: 2,
        name: 'chelsea',
        price: 1900,
        description: 'A football club',
        key: '2131231222'
      },
    ])
  }
}

@Component({template: 'DummyComponent'})
class DummyComponent {}
