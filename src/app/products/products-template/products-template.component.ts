import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { Router } from '@angular/router';

import { ProductService } from 'src/app/products/services/product.service';
import { DeleteProduct } from 'src/app/products/store/product.actions';
import { ProductState } from 'src/app/products/store/product.state';
import { IProducts } from 'src/app/shared/interfaces/productInterface';

@Component({
  selector: 'app-products-template',
  templateUrl: './products-template.component.html',
  styleUrls: ['./products-template.component.css']
})

export class ProductsTemplateComponent implements OnInit, OnDestroy {
  columnsToDisplay = ['id', 'name', 'details', 'update', 'delete']
  @Select(ProductState.getProductList) productsList: Observable<IProducts[]>
  dataSource: IProducts []

  dataSubscription: Subscription
  productActionTypeDelete: string = DeleteProduct.type

  constructor(private productService: ProductService, private router: Router, private store: Store) { }

  ngOnInit(): void {
    this.getProductListFromStore()
  }

  fromFilterComponent(id: number){
    this.getProductListFromStore()
    if(Number.isInteger(id)){
      this.dataSource = this.productService.getProductbyID(id, this.dataSource)
    } else {
      this.getProductListFromStore()
    }
  }

  getProductListFromStore(){
    this.dataSubscription = this.productsList.subscribe(ProductListData => {
      this.dataSource = ProductListData
    })
  }

  onAddNewProduct(){
    this.router.navigate(['/products/new'])
  }

  onUpdate(product: IProducts){
    this.router.navigate(['products/update'], { state: {... product} } )
  }

  onDelete(key: string): Observable<any>{
    return this.store.dispatch(new DeleteProduct(key))
  }

  ngOnDestroy(){
    this.dataSubscription.unsubscribe()
  }

}
