import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsComponent } from './products-main/products.component';
import { ProductsTemplateComponent } from './products-template/products-template.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductFromComponent } from './product-from/product-from.component';

import { AuthGuard } from '../auth/services/auth.guard';
import { UpdateGuardService } from './services/update-guard.service';

const routes: Routes = [
    { path: '', component: ProductsComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard], 
    children: [
        { path: '', component: ProductsTemplateComponent, pathMatch: 'full' },
        { path: 'new', component: ProductFromComponent },
        { path: ':id/details', component: ProductDetailsComponent},
        { path: 'update', component: ProductFromComponent, canActivate: [UpdateGuardService] }
    ]},
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class ProductsRoutingModule{}