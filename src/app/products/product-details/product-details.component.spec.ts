import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ProductDetailsComponent } from './product-details.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ProductService } from '../services/product.service';
import { IProducts } from 'src/app/shared/interfaces/productInterface';


describe('ProductDetailsComponent', () => {
  let component: ProductDetailsComponent;
  let fixture: ComponentFixture<ProductDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MaterialModule, BrowserAnimationsModule, ],
      declarations: [ ProductDetailsComponent ],
      providers: [ 
        { provide: ProductService, useClass: ProductServiceStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  })
  
})

class ProductServiceStub{
  subject = new Subject<IProducts[]>()

  constructor(){
    this.subject.next(this.productsArray)
  }
  
  productsArray = [
    {
      id: 1,
      name: 'arsenal',
      price: 2000,
      description: 'A football club',
      key: '21312312random'
    },
    {
      id: 2,
      name: 'chelsea',
      price: 1900,
      description: 'A football club',
      key: '2131231222'
    },
  ]
}

class RouterStub{
  navigate(params){}
}

class ActivatedRouteStub{
  params: Observable<any> = of()
}
