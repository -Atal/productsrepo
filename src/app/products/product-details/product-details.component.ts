import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';

import { Select } from '@ngxs/store';
import { ProductState } from '../store/product.state';
import { IProducts } from 'src/app/shared/interfaces/productInterface';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})

export class ProductDetailsComponent implements OnInit, OnDestroy {

  @Select(ProductState.getProductList) productsList: Observable<IProducts[]>
  id: number
  productsArray: IProducts[]
  newArray: IProducts
  found: boolean
  isLoading: boolean
  private dataSubscription: Subscription
  private routeIDSubscription: Subscription

  constructor(private route: ActivatedRoute) {}
  
  ngOnInit(): void {
  
    this.isLoading = true
    this.dataSubscription = this.productsList.subscribe(ProductListData => {
      this.productsArray = ProductListData
      this.checkforProduct()
    })
  }

  checkforProduct(){
    this.routeIDSubscription = this.route.params.subscribe( (data) => {
      this.id = +data['id']
      for (let product of this.productsArray){
        if (product.id === this.id){
          this.found = true
          this.newArray = product
          this.isLoading = false
          break;
        } else {
          this.found = false
          this.isLoading = false
        }
      }
    })
  }

  ngOnDestroy(){
    this.dataSubscription.unsubscribe()
    this.routeIDSubscription.unsubscribe()
  }

}
