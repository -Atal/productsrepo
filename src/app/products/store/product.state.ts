import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators'
import { Injectable } from '@angular/core';

import { ProductService } from '../services/product.service';
import { GetAllProducts, AddNewProduct, UpdateProduct, DeleteProduct } from './product.actions';
import { IProducts } from '../../shared/interfaces/productInterface';

export interface ProductStateModel{
    products: IProducts[]
    isLoading: boolean
}

@State<ProductStateModel>({
    name: 'products',
    defaults: {
        products: [],
        isLoading: false
    }
})

@Injectable()
export class ProductState {
    constructor(private productService: ProductService) {}

    @Selector()
    static getProductList(state: ProductStateModel) {
        return state.products
    }

    @Action(GetAllProducts)
    getAllProducts({ getState, setState }: StateContext<ProductStateModel> ) {
        return this.productService.getAllProducts().pipe(
            tap((data) => {
                let products: IProducts[]
                products = []
                if(data.length !== 0){
                    for (let i=data.length-1; i>=0; i--){
                        products[i] = data[i].payload.doc.data()
                        //need this key for update/delete
                        products[i].key = data[i].payload.doc.id
                    }
                }
                const state = getState()
                setState({
                    ...state,
                    products: products
                })
            })
        )
    }

    @Action(AddNewProduct)
    async addNewProduct({ getState, patchState}: StateContext<ProductStateModel>, { payload }) {
        const newProduct = payload
        const state = getState()
        patchState({
            isLoading: true
        })
        await this.productService.addProduct(newProduct).then((resp) => {
            newProduct.key = resp.id
            patchState({
                products: [...state.products, newProduct],
                isLoading: false
            })  
        })   
    }

    @Action(UpdateProduct)
    async updateProduct({ getState, setState }: StateContext<ProductStateModel>, { payload }) {
        const updatedProduct: IProducts = payload
        const state = getState()
        const productList = [...state.products]
        let updatedProductIndex = null
        productList.filter((product, index) => {
            if (product.id === updatedProduct.id){
                updatedProductIndex = index
            }
        })
        productList[updatedProductIndex] = updatedProduct
        await this.productService.updateProduct(updatedProduct).then(() => {
            setState({
                ...state,
                products: productList
            })
        }) 
    }

    @Action(DeleteProduct)
    async deleteProduct({ getState, setState }: StateContext<ProductStateModel>, { payload } ) {
        const state = getState()
        const filteredArray = state.products.filter(product => product.key !== payload)
        await this.productService.deleteProduct(payload).then(() => {
            setState({
                ...state,
                products: filteredArray
            })
        })
    }

}

