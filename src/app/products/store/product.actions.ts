import { IProducts } from '../../shared/interfaces/productInterface';

export class GetAllProducts {
    static readonly type = '[Product] Get All Products'
}

export class AddNewProduct {
    static readonly type = '[Product] Add New Product'
    constructor(public payload: IProducts) {}
}


export class UpdateProduct {
    static readonly type = '[Product] Update Product'
    constructor(public payload: IProducts) {}
}

export class DeleteProduct {
    static readonly type = '[Product] Delete Product'
    constructor(public payload: string) {}
}