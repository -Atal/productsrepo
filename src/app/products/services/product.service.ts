import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { IProducts } from '../../shared/interfaces/productInterface';

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  
  constructor(private firestoreService: AngularFirestore) {}

  getAllProducts() {
    return this.firestoreService.collection('Products').snapshotChanges();
  }

  addProduct(newProduct: IProducts){
    const productObject = {...newProduct};
    return this.firestoreService.collection('Products').add(productObject);
  }

  updateProduct(product: IProducts){
    const productObject = {...product};
    delete productObject.key
    return this.firestoreService.doc('Products/' + product.key).update(productObject)
  }

  deleteProduct(productKey: string) {
    return this.firestoreService.doc('Products/' + productKey).delete()
  }

  getProductbyID(id: number, products: IProducts[]){
    let filterProducts = []
    for(let product of products){
      if(product.id.toString().includes(id.toString())){
        filterProducts.push(product)
      }
    }
    return filterProducts
  }

}
