import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UpdateGuardService implements CanActivate {

  hasData: boolean

  constructor(private router: Router) { 
    const navigation = router.getCurrentNavigation()
    const state = navigation.extras.state
    
    if(state){
      this.hasData = true
    } else {
      this.hasData = false
    }
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
  Observable<boolean>| Promise<boolean>| boolean{

    if (this.hasData){
      return true
    } else {
      this.router.navigate(['/products'])
    }
  }
}
