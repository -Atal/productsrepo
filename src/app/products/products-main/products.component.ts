import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

import { GetAllProducts } from '../store/product.actions';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.store.dispatch(new GetAllProducts())
  }

}
