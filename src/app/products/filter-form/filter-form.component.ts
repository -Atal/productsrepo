import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-filter-form',
  templateUrl: './filter-form.component.html',
  styleUrls: ['./filter-form.component.css']
})
export class FilterFormComponent implements OnInit {

  @ViewChild('a') myForm: NgForm
  
  @Output() change = new EventEmitter()

  id: number
  constructor() { }

  ngOnInit(): void {
  }

  onChangeID(id: number){
    this.change.emit(id)
  }

}
