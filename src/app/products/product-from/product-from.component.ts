import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CloudinaryWidgetManager } from 'ngx-cloudinary-upload-widget';

import { IProducts } from 'src/app/shared/interfaces/productInterface';
import { ProductState } from '../store/product.state';
import { AddNewProduct, UpdateProduct } from '../store/product.actions';

declare var cloudinary: any;

@Component({
  selector: 'app-product-from',
  templateUrl: './product-from.component.html',
  styleUrls: ['./product-from.component.css']
})
export class ProductFromComponent implements OnInit {

  myForm: FormGroup;
  editMode: boolean = false
  products: IProducts[]
  tempProduct: IProducts
  watchAddProductAction: string = AddNewProduct.type
  watchUpdateProductAction: string = UpdateProduct.type
  productImageUrl: string = null
  myWidget
  cloudinaryConfig: object = {
    uploadPreset: 'bh2dblt5',
    sources: ['local', 'url'],
    styles: {
      palette: {
          window: "#5D005D",
          sourceBg: "#3A0A3A",
          windowBorder: "#AD5BA3",
          tabIcon: "#ffffcc",
          inactiveTabIcon: "#FFD1D1",
          menuIcons: "#FFD1D1",
          link: "#ffcc33",
          action: "#ffcc33",
          inProgress: "#00e6b3",
          complete: "#a6ff6f",
          error: "#ff1765",
          textDark: "#3c0d68",
          textLight: "#fcfffd"
      }
    }
  }

  @Select(ProductState.getProductList) productsList: Observable<IProducts[]>

  constructor(private router: Router, private store: Store, private cloudinaryWidgetmanager: CloudinaryWidgetManager) {
    
    // this.myWidget = cloudinary.createUploadWidget({
    //   cloudName: 'atal', 
    //   uploadPreset: 'bh2dblt5',
    //   sources: ['local', 'url'],
    // }, (error, result) => { 
    //     if (!error && result && result.event === "success") { 
    //       console.log('Done! Here is the image info: ', result.info); 
    //     }
    //   }
    // )

    if (this.router.url === '/products/update'){
      const navigation = this.router.getCurrentNavigation()
      this.tempProduct = navigation.extras.state

      if(this.tempProduct){
        this.productImageUrl = this.tempProduct.imageUrl
        this.editMode = true
      }
    }
  }

  ngOnInit(): void {
    this.productsList.subscribe(productListData => {
      this.products = productListData
    })

    this.myForm = new FormGroup ({
      'name': new FormControl(null, Validators.required),
      'price': new FormControl(null, Validators.required),
      'description': new FormControl(null, Validators.required)
    })

    if(this.editMode){
      this.myForm.patchValue({
        'name': this.tempProduct.name,
        'price': this.tempProduct.price,
        'description': this.tempProduct.description
      })
    }
  }

  onSubmit(): Observable<any>{
    if(this.editMode){
      return this.updateProductLogic()
    } else {
      return this.newProductLogic()
    }
  }

  newProductLogic(): Observable<any>{
    let max = 0
    for (let product of this.products){
      if (product.id > max){
        max = product.id
      }
    }
    max++

    const newProduct:IProducts = {
      id: max,
      name: this.myForm.controls['name'].value,
      price: this.myForm.controls['price'].value,
      description: this.myForm.controls['description'].value,
      imageUrl: this.productImageUrl
    }
    
    return this.store.dispatch(new AddNewProduct(newProduct)).pipe(tap(resp => {
      this.router.navigate(['/products'])
    }))
    
  }

  updateProductLogic(): Observable<any>{
    const updatedProduct: IProducts = {
      id: this.tempProduct.id,
      key: this.tempProduct.key,
      name: this.myForm.controls['name'].value,
      price: this.myForm.controls['price'].value,
      description: this.myForm.controls['description'].value,
      imageUrl: this.productImageUrl
    }

    return this.store.dispatch(new UpdateProduct(updatedProduct)).pipe(tap(resp => {
      this.editMode = false
      this.router.navigate(['/products'])
    }))
  }

  onOpen(){
    // this.myWidget.open()

    this.cloudinaryWidgetmanager.open(this.cloudinaryConfig).subscribe((resp)=> {
      console.log('From success',resp)
      console.log(typeof(resp))
      if(resp.event === 'success'){
        if(resp.info && resp.info.url){
          this.productImageUrl = resp.info.url
          console.log(this.productImageUrl)
        }
      }
    }, (error)=>{
      console.log('From Error', error)
    })

  }

}
