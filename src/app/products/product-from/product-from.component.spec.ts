import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ProductFromComponent } from './product-from.component';
import { ProductService } from '../services/product.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/shared/material/material.module';

describe('ProductFromComponent', () => {
  let component: ProductFromComponent;
  let fixture: ComponentFixture<ProductFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductFromComponent ],
      imports: [ RouterTestingModule, MaterialModule, BrowserAnimationsModule, ReactiveFormsModule ],
      providers: [ 
        { provide: ProductService, useClass: ProductServiceStub },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  })

})

export class ProductServiceStub{}
