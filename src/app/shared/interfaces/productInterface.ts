export interface IProducts {
    id?: number
    name?: string
    price?: number
    description?: string
    key?: string
    imageUrl? : string
}