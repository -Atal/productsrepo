import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';

import { User } from 'src/app/auth/models/user.model';
import { Logout } from 'src/app/auth/store/auth.actions';
import { AuthState } from 'src/app/auth/store/auth.state';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private userSub: Subscription
  isAuthenticated: boolean = false

  @Select(AuthState.getUser) user: Observable<User>

  constructor(private router: Router, private store: Store) { }

  ngOnInit(): void {
    this.userSub = this.user.subscribe((respData) => {
      this.isAuthenticated = !!respData
    })
  }

  onHomeRedirect(){
    if(this.isAuthenticated){
      this.router.navigate(['/products'])
    } else {
      this.router.navigate(['/auth'])
    }
  }

  onLogout(){
    this.store.dispatch(new Logout())
    this.router.navigate(['/auth'])
  }

  ngOnDestroy(){
    this.userSub.unsubscribe()
  }

}
