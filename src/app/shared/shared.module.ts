import { NgModule } from "@angular/core";

import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { HeaderComponent } from './header/header.component';

import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';

@NgModule({
    declarations: [
        LoadingSpinnerComponent,
        HeaderComponent,
    ],

    imports: [
        CommonModule,
        MaterialModule,
    ],

    exports: [
        CommonModule,
        MaterialModule,
        LoadingSpinnerComponent,
        HeaderComponent,
    ]

})

export class SharedModule{}