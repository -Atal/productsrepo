export class Login {
    static readonly type = '[Auth] Login'
    constructor(public email: string, public password: string){}
}

export class SignUp {
    static readonly type = '[Auth] Signup'
    constructor(public email: string, public password: string){}
}

export class Logout {
    static readonly type = '[Auth] Logout'
    constructor(){}
}

export class AutoLogin {
    static readonly type = '[Auth] Auto Login'
    constructor(){}
}

export class AutoLogout {
    static readonly type = '[Auth] Auto Logout'
    constructor(){}
}