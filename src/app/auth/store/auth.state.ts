import { State, Action, StateContext, Selector, Store } from "@ngxs/store";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';

import { AutoLogin, Login, Logout, SignUp } from './auth.actions';
import { User } from '../models/user.model';

export interface AuthResponseData{
    kind: string
    idToken: string
    email: string
    refreshToken: string
    expiresIn: string
    localId: string
    registered: boolean
}

interface AuthForm {
    newAuthForm: {
        model?: {
            username: string;
            password: string;
        }
        dirty: false;
        status: '';
        errors: {}
    }
}

interface AuthStateModel{
    user: User
    authForm: AuthForm
}

@State<AuthStateModel>({
    name: 'auth',
    defaults: {
        user: null,
        authForm: {
            newAuthForm: {
                model: {
                    username: null,
                    password: null
                },
                dirty: false,
                status: '',
                errors: {}
            }
        }
    }
})

@Injectable()
export class AuthState{
    private tokenExpirationTimer

    constructor(private http: HttpClient, private store: Store, private router: Router){}

    @Selector()
    static getUser(state: AuthStateModel) {
        return state.user
    }

    @Selector()
    static getFormState(state: AuthStateModel) {
        return state.authForm
    }

    @Selector()
    static getFormErrors(state: AuthStateModel) {
        return state.authForm.newAuthForm.errors
    }

    @Action(SignUp)
    onSignUp({ getState, patchState }: StateContext<AuthStateModel>, payload){
        const { email, password } = payload
        return this.http.post<AuthResponseData>(
            'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCCt1l6avbwNpkQR3dCxSNRR0503Q73OvU',
            {
                email: email,
                password: password,
                returnSecureToken: true
            }
        ).pipe(catchError(this.handleErrors), tap((respData)=>{
            const { email, localId, idToken, expiresIn } = respData
            patchState({
                user: this.handleAuthentication(email, localId, idToken, expiresIn)
            })
        }))
    }

    @Action(Login)
    onLogin({ getState, setState ,patchState }: StateContext<AuthStateModel>, payload){
        const { email, password } = payload
        return this.http.post<AuthResponseData>(
            'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCCt1l6avbwNpkQR3dCxSNRR0503Q73OvU',
            {
                email: email,
                password: password,
                returnSecureToken: true
            }
        ).pipe( tap((respData)=>{
            const { email, localId, idToken, expiresIn } = respData
            patchState({
                user: this.handleAuthentication(email, localId, idToken, expiresIn)
            })
        }, (errorResp) => {
            const state = getState();
            setState({
                ...state,
                authForm: {
                    newAuthForm: {
                        ...state.authForm.newAuthForm,
                        errors: {email: errorResp.error.error.message}
                    }
                }
            })
            throw errorResp.error
        })) 
    }

    @Action(Logout)
    onLogout({ patchState }: StateContext<AuthStateModel>){
        localStorage.removeItem('currentUserData')
        if(this.tokenExpirationTimer){
            clearTimeout(this.tokenExpirationTimer)
        }
        this.tokenExpirationTimer = null
        patchState({
            user: null
        })
        // this.router.navigate(['/auth'])
    }

    @Action(AutoLogin)
    onAutoLogin({ getState, patchState }: StateContext<AuthStateModel>){
        let userData: {email: string, id: string, _token: string, _tokenExpirationDate: string} 
        = JSON.parse(localStorage.getItem('currentUserData'))
        if(!userData){
            return
        }
        const { email, id, _token, _tokenExpirationDate } = userData
        const loadedUser = new User(email, id, _token, new Date(_tokenExpirationDate))

        if(loadedUser.getToekn()){
            patchState({
                user: loadedUser
            })
        const expirationDuration = new Date(_tokenExpirationDate).getTime() - new Date().getTime()
        this.autoLogout(expirationDuration)
        }
    }

    private autoLogout(expirationDuration: number){
        this.tokenExpirationTimer = setTimeout(()=> {
            this.store.dispatch(new Logout())
        }, expirationDuration)
    }

    private handleErrors(errorResp: HttpErrorResponse){
        let errorMessage = 'An unknown error occurred'
        if(!errorResp.error || !errorResp.error.error){
            return throwError(errorMessage)
        }
        switch (errorResp.error.error.message) {
            case 'EMAIL_EXISTS':
                errorMessage = 'Email already exists'
                break;
            case 'EMAIL_NOT_FOUND':
                errorMessage = 'No account with this email'
                break;
            case 'INVALID_PASSWORD':
                errorMessage = 'Incorrect Password'
                break
            default:
                errorMessage = 'An unknown error occurred'
                break;
            }
        return throwError(errorMessage)
    }

    private handleAuthentication(email: string, userId: string, token: string, expiresIn: string){
        const expirationDate = new Date(new Date().getTime() + +expiresIn * 1000)
        const currentUser = new User(email, userId, token, expirationDate)
        this.autoLogout(+expiresIn * 1000)
        localStorage.setItem('currentUserData', JSON.stringify(currentUser))
        return currentUser
    }

}