import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { AuthResponseData } from '../../services/auth.service';
import { Select, Store } from '@ngxs/store';
import { Login, SignUp } from '../../store/auth.actions';
import { AuthState } from '../../store/auth.state';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup
  loginMode: Boolean = true
  isLoading: Boolean = false
  errorMessage: string = null
  private userSub: Subscription

  @Select(AuthState.getUser) currentUser: Observable<User>
  @Select(AuthState.getFormState) form$: Observable<any>
  @Select(AuthState.getFormErrors) errors$: Observable<any>

  constructor(private router: Router, private store: Store) { }
  
  ngOnInit(): void {
    this.loginForm = new FormGroup ({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),
    })
    this.userSub = this.currentUser.subscribe((data) => {
      if(data){
        this.router.navigate(['/products'])
      }
    })
  }

  onSubmit(){
    this.errors$.subscribe(resp => {
      if(resp){
        console.log('From errors',resp)
      }
    })
    const { email, password } = this.loginForm.value
    let authObs: Observable<AuthResponseData>
    this.isLoading = true

    if(this.loginMode){
      authObs = this.store.dispatch(new Login(email, password))
    } else {
      authObs = this.store.dispatch(new SignUp(email, password))
    }

    authObs.subscribe((respData) => {
      this.isLoading = false
      this.router.navigate(['/products'])
    }, (errorMessageResp) => {
      this.isLoading = false
      this.errorMessage = errorMessageResp
    })
  }

  switch(){
    this.loginMode = !this.loginMode
  }

  ngOnDestroy(){
    this.userSub.unsubscribe()
  }

}
