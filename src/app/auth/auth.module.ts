import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';

import { LoginComponent } from './pages/login/login.component';
import { SharedModule } from '../shared/shared.module';
import { AuthsRoutingModule } from './auth-routing.module';
import { AuthState } from './store/auth.state';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';

@NgModule({
    declarations: [
        LoginComponent,
    ],

    imports: [
        SharedModule,
        ReactiveFormsModule,
        AuthsRoutingModule,
        NgxsFormPluginModule,
        // NgxsModule.forFeature([AuthState])
    ],

    exports: [
        LoginComponent
    ],
})

export class AuthModule {}