import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { Store } from '@ngxs/store';
import { User } from '../models/user.model';

@Injectable({
    providedIn: 'root'
})

export class AuthGuard implements CanActivate, CanActivateChild{

    constructor(private router: Router, private store: Store){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
        return this.store.selectOnce(state => state.auth.user).pipe(
            map((user: User) => {
                const isAuth = !!user
                if(isAuth){
                    return true
                }
                return this.router.createUrlTree(['/auth'])
            }))
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
        return this.canActivate(route, state)
    }
    
}