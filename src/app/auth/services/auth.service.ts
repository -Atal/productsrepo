import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

export interface AuthResponseData{
    kind: string
    idToken: string
    email: string
    refreshToken: string
    expiresIn: string
    localId: string
    registered: boolean
}

@Injectable({
    providedIn: 'root'
})

export class AuthService{

    constructor(private router: Router){}

    // autoLogin(){
    //     let userData: {email: string, id: string, _token: string, _tokenExpirationDate: string} 
    //     = JSON.parse(localStorage.getItem('currentUserData'))
    //     if(!userData){
    //         return
    //     }
    //     const { email, id, _token, _tokenExpirationDate } = userData
    //     const loadedUser = new User(email, id, _token, new Date(_tokenExpirationDate))

    //     if(loadedUser.getToekn()){
    //         this.user.next(loadedUser)
    //         const expirationDuration = new Date(_tokenExpirationDate).getTime() - new Date().getTime()
    //         this.autoLogout(expirationDuration)
    //         this.router.onSameUrlNavigation
    //     }
    // }

    // logout(){
    //     this.user.next(null)
    //     this.router.navigate(['/auth'])
    //     localStorage.removeItem('currentUserData')

    //     if(this.tokenExpirationTimer){
    //         clearTimeout(this.tokenExpirationTimer)
    //     }
    //     this.tokenExpirationTimer = null
    // }

    // autoLogout(expirationDuration: number){
    //     this.tokenExpirationTimer = setTimeout(()=> {
    //         this.logout()
    //     }, expirationDuration)
    // }

}