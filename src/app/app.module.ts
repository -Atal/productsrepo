import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { NgxsModule } from '@ngxs/store';
import { NgxsLoadingPluginModule } from 'ngxs-loading-plugin';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';

import { AngularFireModule } from '@angular/fire'
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from 'src/environments/environment';

import { AppComponent } from './app.component';
import { AuthState } from './auth/store/auth.state';

import { CloudinaryModule } from '@cloudinary/angular-5.x';
import * as  Cloudinary from 'cloudinary-core';
import { NgxCloudinaryWidgetModule } from 'ngx-cloudinary-upload-widget';

@NgModule({
  declarations: [
    AppComponent,
  ],

  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    NgxsModule.forRoot([AuthState], { developmentMode: !environment.production }),
    NgxsFormPluginModule.forRoot(),
    NgxsLoadingPluginModule.forRoot(),
    // ReactiveFormsModule,
    // FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CloudinaryModule.forRoot(Cloudinary, 
      {
        cloud_name: 'atal',
        upload_preset: 'bh2dblt5',
      }
    ),
    NgxCloudinaryWidgetModule.forRoot(
      {
          cloudName:"atal"
      }
    )
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
